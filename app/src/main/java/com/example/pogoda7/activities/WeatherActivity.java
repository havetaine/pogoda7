package com.example.pogoda7.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pogoda7.R;
import com.example.pogoda7.api.JsonPlaceholderAPI;
import com.example.pogoda7.api.WeatherData;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherActivity extends AppCompatActivity
{
    private TextView cityNameView;
    private TextView timeView;
    private TextView tempView;
    private TextView pressureView;
    private TextView humidityView;
    private TextView tempMinView;
    private TextView tempMaxView;
    private ImageView weatherIconView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        cityNameView = findViewById(R.id.cityName);
        timeView = findViewById(R.id.timeView);
        tempView = findViewById(R.id.tempView);
        pressureView = findViewById(R.id.pressureView);
        humidityView = findViewById(R.id.humidityView);
        tempMinView = findViewById(R.id.tempMinView);
        tempMaxView = findViewById(R.id.tempMaxView);
        weatherIconView = findViewById(R.id.weatherIconView);

        Intent intent = getIntent();
        String name = intent.getStringExtra("CITY_NAME");
        
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        timeView.setText(formatter.format(date));

        final SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.refreshLayout);
        swipeRefreshLayout.setOnRefreshListener(() ->
        {
            getWeatherData(name);
            swipeRefreshLayout.setRefreshing(false);
        });

        getWeatherData(name + ",pl");
    }

    private void getWeatherData(String cityName)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceholderAPI jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI.class);

        Call<WeatherData> call = jsonPlaceholderAPI.getData(cityName,"749561a315b14523a8f5f1ef95e45864", "metric");
        System.out.println("URL: " + call.request().url().toString());
        call.enqueue(new Callback<WeatherData>()
        {
            @Override
            public void onResponse(Call<WeatherData> call, Response<WeatherData> response)
            {
                if(!response.isSuccessful())
                {
                    Toast toast = Toast.makeText(getApplicationContext(), "Incorrect city name.", Toast.LENGTH_SHORT);
                    toast.show();
                    finish();
                    return;
                }

                WeatherData weatherData = response.body();

                cityNameView.setText("City\n" + weatherData.getName());
                tempView.append(Html.fromHtml("<font color=\"black\"><big><br>" + weatherData.getMain().getTemp() + " °C</big></font>"));
                pressureView.append(Html.fromHtml("<font color=\"black\"><big><br>" + weatherData.getMain().getPressure() + " hPa</big></font>"));
                humidityView.append(Html.fromHtml("<font color=\"black\"><big><br>" + weatherData.getMain().getHumidity() + " %</big></font>"));
                tempMinView.append(Html.fromHtml("<font color=\"black\"><big><br>" + weatherData.getMain().getTemp_min() + " °C</big></font>"));
                tempMaxView.append(Html.fromHtml("<font color=\"black\"><big><br>" + weatherData.getMain().getTemp_max() + " °C</big></font>"));
                System.out.println(weatherData.getWeatherList().get(0).getIcon());
                Picasso.get().load("https://openweathermap.org/img/wn/" + weatherData.getWeatherList().get(0).getIcon() + "@2x.png").into(weatherIconView);
            }

            @Override
            public void onFailure(Call<WeatherData> call, Throwable t)
            {
                Toast toast = Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT);
                toast.show();
                finish();
            }
        });
    }
}