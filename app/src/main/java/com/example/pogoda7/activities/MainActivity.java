package com.example.pogoda7.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pogoda7.R;
import com.example.pogoda7.util.CheckInternet;

public class MainActivity extends AppCompatActivity
{
    private Button changeActivity;
    private EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = findViewById(R.id.cityInput);
        changeActivity = findViewById(R.id.weatherButton);

        if(!CheckInternet.isNetworkConnected(this) || CheckInternet.isInternetAvailable())
        {
            changeActivity.setEnabled(false);
            Toast toast = Toast.makeText(getApplicationContext(), "No internet conenction.", Toast.LENGTH_SHORT);
            toast.show();
        }

        SharedPreferences sharedPreferences = getSharedPreferences("shared preferneces for pogoda7", MODE_PRIVATE);
        String name = sharedPreferences.getString("CITY_NAME", "Warszawa");
        input.setText(name);
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    public void saveCityName(View view)
    {
        String cityName = input.getText().toString();
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferneces for pogoda7", MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("CITY_NAME", cityName);
        editor.apply();

        Intent intent = new Intent(this, WeatherActivity.class);
        intent.putExtra("CITY_NAME", cityName);
        startActivity(intent);
    }
}