package com.example.pogoda7.api;

import com.example.pogoda7.Main;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherData
{
    private String name;
    @SerializedName("main")
    private Main main;
    @SerializedName("weather")
    private List<Weather> weatherList;

    public WeatherData(String name, double temp, double humidity, double pressure, double temp_min, double temp_max)
    {
        this.name = name;
        this.main = new Main(temp, humidity, pressure, temp_min, temp_max);
    }

    public String getName()
    {
        return name;
    }

    public Main getMain()
    {
        return main;
    }

    public List<Weather> getWeatherList()
    {
        return weatherList;
    }
}
