package com.example.pogoda7.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JsonPlaceholderAPI
{
    @GET("data/2.5/weather")
    Call<WeatherData> getData(@Query("q") String q, @Query("APPID") String app_id, @Query("units") String units);
}
