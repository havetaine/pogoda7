package com.example.pogoda7;

public class Main
{
    private double temp;
    private double humidity;
    private double pressure;
    private double temp_min;
    private double temp_max;

    public Main(double temp, double humidity, double pressure, double temp_min, double temp_max)
    {
        this.temp = temp;
        this.humidity = humidity;
        this.pressure = pressure;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
    }

    public double getTemp() {
        return temp;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getPressure() {
        return pressure;
    }

    public double getTemp_min() {
        return temp_min;
    }

    public double getTemp_max() {
        return temp_max;
    }
}
