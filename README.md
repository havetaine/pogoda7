# pogoda7 ☁️

**pogoda7** is an Android application to check the weather ☁️☀️ of a specific location. The weather data is provided by [OpenWeatherMap](openweathermap.org). The aplication was created in Android Studio using Java.  

## Application
### Opening window
Users can enter the name of the city for which they want to check the weather.  

![](screenshots/input_screen.png)  
### Results
The screen where weather (including current temperature, pressure, humidty, min and max temperature) and time are displayed. 
User can refresh the data by swiping down.  

![](screenshots/weather_screen.png)  

## Technology stack
- Java 17
- Android 6.0+
- HTTP client - [Retrofit](https://square.github.io/retrofit/)
- JSON conversion - [Gson](https://github.com/google/gson)
- Image library used for displaying weather icon - [Picasso](https://square.github.io/picasso/)